package com.example.financije

import android.os.Bundle
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class UkupnoActivity : AppCompatActivity() {

    private lateinit var expenseDao: ExpenseDao

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_ukupno)

        expenseDao = MyApp.getDatabase(this).expenseDao()

        // Display the sum of expenses for each type of expense
        displayExpenseSumForTypes()
    }



    private fun displayExpenseSumForTypes() {
        GlobalScope.launch(Dispatchers.IO) {
            val sumHrana = expenseDao.getExpenseSumByType(4)
            val sumRezije = expenseDao.getExpenseSumByType(5)
            val sumOstalo = expenseDao.getExpenseSumByType(6)

            withContext(Dispatchers.Main) {
                val textViewHrana: TextView = findViewById(R.id.textViewHrana)
                val textViewRezije: TextView = findViewById(R.id.textViewRezije)
                val textViewOstalo: TextView = findViewById(R.id.textViewOstalo)

                // Display the sum of expenses for each type
                textViewHrana.text = "Hrana: $sumHrana EUR"
                textViewRezije.text = "Režije: $sumRezije EUR"
                textViewOstalo.text = "Ostalo: $sumOstalo EUR"
            }
        }
    }


}
