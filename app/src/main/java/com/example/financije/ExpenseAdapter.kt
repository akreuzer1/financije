package com.example.financije
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.financije.R
import com.example.financije.Expense

class ExpenseAdapter : RecyclerView.Adapter<ExpenseAdapter.ExpenseViewHolder>() {
    private val expenses = ArrayList<Expense>()
    private val typeOfExpenses = ArrayList<TypeOfExpense>() // Added TypeOfExpense list

    private var onItemClickListener: ((Expense) -> Unit)? = null
    private var onDeleteClickListener: ((Expense) -> Unit)? = null

    fun setOnDeleteClickListener(listener: (Expense) -> Unit) {
        onDeleteClickListener = listener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ExpenseViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.item_expense, parent, false)
        return ExpenseViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: ExpenseViewHolder, position: Int) {
        val expense = expenses[position]
        holder.bind(expense)

        holder.itemView.setOnClickListener {
            onItemClickListener?.invoke(expense)
        }
    }

    fun setOnItemClickListener(listener: (Expense) -> Unit) {
        onItemClickListener = listener
    }

    override fun getItemCount(): Int {
        return expenses.size
    }

    fun setData(expenseList: List<Expense>) {
        expenses.clear()
        expenses.addAll(expenseList)
        notifyDataSetChanged()
    }

    fun setTypeOfExpenses(typeOfExpenseList: List<TypeOfExpense>) {
        typeOfExpenses.clear()
        typeOfExpenses.addAll(typeOfExpenseList)
    }
    fun getTypeOfExpenses(): List<TypeOfExpense> {
        return typeOfExpenses
    }

    inner class ExpenseViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val textViewName: TextView = itemView.findViewById(R.id.textViewName)
        private val textViewAmount: TextView = itemView.findViewById(R.id.textViewAmount)
        private val textViewType: TextView = itemView.findViewById(R.id.typeExpense)
        private val buttonDelete: Button = itemView.findViewById(R.id.buttonDelete)

        fun bind(expense: Expense) {
            textViewName.text = expense.name
            textViewAmount.text = "${expense.amount} €"

            val typeOfExpenseId = expense.typeOfExpenseId
            val typeOfExpenseName = getTypeOfExpenseName(typeOfExpenseId)
            textViewType.text = typeOfExpenseName

            itemView.setOnClickListener {
                onItemClickListener?.invoke(expense)
            }

            buttonDelete.setOnClickListener {
                onDeleteClickListener?.invoke(expense)
            }
        }

        private fun getTypeOfExpenseName(typeOfExpenseId: Long): String {
            return when (typeOfExpenseId) {
                4L -> "Hrana"
                5L -> "Režije"
                6L -> "Ostalo"
                else -> ""
            }
        }

    }
}
