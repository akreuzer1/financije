package com.example.financije
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.Ignore
import androidx.room.PrimaryKey


@Entity(tableName = "expenses",
    foreignKeys = [
        ForeignKey(
            entity = TypeOfExpense::class,
            parentColumns = ["id"],
            childColumns = ["typeOfExpenseId"],
            onDelete = ForeignKey.CASCADE
        )
    ]
)
data class Expense(
    @PrimaryKey(autoGenerate = true)
    val id: Long = 0,
    val name: String,
    val amount: Double,
    val typeOfExpenseId: Long,

)