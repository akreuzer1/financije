package com.example.financije

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update

@Dao
interface ExpenseDao {
    @Query("SELECT * FROM expenses")
    fun getAllExpenses(): List<Expense>

    @Insert
    fun addExpense(expense: Expense)

    @Delete
    fun deleteExpense(expense: Expense)

    @Query("SELECT * FROM expenses WHERE id = :expenseId")
    fun getExpenseById(expenseId: Long): Expense

    @Update
    fun updateExpense(expense: Expense)

    @Query("SELECT SUM(amount) FROM expenses WHERE typeOfExpenseId = :typeOfExpenseId")
    fun getExpenseSumByType(typeOfExpenseId: Long): Double

}
