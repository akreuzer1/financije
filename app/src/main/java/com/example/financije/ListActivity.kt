package com.example.financije
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.EditText
import android.widget.Spinner
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.financije.R
import com.example.financije.Expense
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext


class ListActivity : AppCompatActivity() {
    private lateinit var typeOfExpenseDao: TypeOfExpenseDao
    private lateinit var expenseDao: ExpenseDao

    private lateinit var expenseAdapter: ExpenseAdapter

    private lateinit var editTextName: EditText
    private lateinit var editTextAmount: EditText
    private lateinit var spinnerTypeOfExpense: Spinner
    private lateinit var buttonAddExpense: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list)


        expenseDao = MyApp.getDatabase(this).expenseDao()
        typeOfExpenseDao = MyApp.getDatabase(this).typeOfExpenseDao() // Initialize TypeOfExpenseDao

        // Inicijalizacija RecyclerView
        val recyclerView: RecyclerView = findViewById(R.id.recyclerView)
        recyclerView.layoutManager = LinearLayoutManager(this)
        expenseAdapter = ExpenseAdapter()
        recyclerView.adapter = expenseAdapter

        // Inicijalizacija UI elemenata
        editTextName = findViewById(R.id.editTextName)
        editTextAmount = findViewById(R.id.editTextAmount)
        spinnerTypeOfExpense = findViewById(R.id.spinnerExpenseType) // Initialize Spinner
        buttonAddExpense = findViewById(R.id.buttonAddExpense)

        // Dohvaćanje svih tipova troškova i postavljanje u adapter
        getAllTypeOfExpenses()

        // Postavljanje slušatelja za gumb "Add Expense"
        buttonAddExpense.setOnClickListener {
            addExpense()
        }

        // Dohvaćanje svih troškova i prikaz na RecyclerView-u
        getAllExpenses()

        expenseAdapter.setOnDeleteClickListener { expense ->
            deleteExpense(expense)
        }

        expenseAdapter.setOnItemClickListener { expense ->
            editExpense(expense)
        }
    }

    private fun getAllExpenses() {
        GlobalScope.launch(Dispatchers.IO) {
            val expenses = expenseDao.getAllExpenses()
            withContext(Dispatchers.Main) {
                expenseAdapter.setData(expenses)
            }
        }
    }

    private var typeOfExpenses: List<TypeOfExpense> = emptyList()

    private fun getAllTypeOfExpenses() {
        GlobalScope.launch(Dispatchers.IO) {
            typeOfExpenses = typeOfExpenseDao.getAllTypeOfExpenses()
            withContext(Dispatchers.Main) {
                // Create an array of expense type names
                val expenseTypeNames = typeOfExpenses.map { it.name }.toTypedArray()

                // Create an adapter and set it on the Spinner
                val adapter = ArrayAdapter(
                    this@ListActivity,
                    android.R.layout.simple_spinner_item,
                    expenseTypeNames
                )
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                spinnerTypeOfExpense.adapter = adapter

                // Set a selection listener to get the selected expense type
                spinnerTypeOfExpense.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                        val selectedTypeOfExpense = typeOfExpenses[position]
                        // Do something with the selected expense type
                    }

                    override fun onNothingSelected(parent: AdapterView<*>?) {
                        // Handle case when nothing is selected
                    }
                }
            }
        }
    }

    private fun addExpense() {
        val name = editTextName.text.toString()
        val amount = editTextAmount.text.toString().toDouble()
        val selectedTypePosition = spinnerTypeOfExpense.selectedItemPosition

        if (typeOfExpenses.isNotEmpty() && selectedTypePosition >= 0 && selectedTypePosition < typeOfExpenses.size) {
            val selectedTypeOfExpense = typeOfExpenses[selectedTypePosition]
            val expense = Expense(name = name, amount = amount, typeOfExpenseId = selectedTypeOfExpense.id)

            Log.d("ListActivity", "Type of Expenses Size: ${typeOfExpenses.size}")

            GlobalScope.launch(Dispatchers.IO) {
                expenseDao.addExpense(expense)
                withContext(Dispatchers.Main) {
                    editTextName.text.clear()
                    editTextAmount.text.clear()
                    getAllExpenses()
                }
            }
        }

    }



    private fun deleteExpense(expense: Expense) {
        GlobalScope.launch(Dispatchers.IO) {
            expenseDao.deleteExpense(expense)
            withContext(Dispatchers.Main) {
                getAllExpenses()
            }
        }
    }

    private val expenseClickListener: (Expense) -> Unit = { expense ->
        editExpense(expense)
    }

    private fun editExpense(expense: Expense) {
        val intent = EditActivity.newIntent(this, expense.id)
        startActivity(intent)
    }
}
