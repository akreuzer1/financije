package com.example.financije
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class EditActivity : AppCompatActivity() {
    private lateinit var expenseDao: ExpenseDao
    private lateinit var typeOfExpenseDao: TypeOfExpenseDao // Added TypeOfExpenseDao
    private var expenseId: Long = -1
    private lateinit var editTextName: EditText
    private lateinit var editTextAmount: EditText
    private lateinit var spinnerTypeOfExpense: Spinner // Added Spinner
    private lateinit var buttonSave: Button

    companion object {
        private const val EXTRA_EXPENSE_ID = "extra_expense_id"

        fun newIntent(context: Context, expenseId: Long): Intent {
            val intent = Intent(context, EditActivity::class.java)
            intent.putExtra(EXTRA_EXPENSE_ID, expenseId)
            return intent
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit)

        editTextName = findViewById(R.id.editTextName)
        editTextAmount = findViewById(R.id.editTextAmount)
        spinnerTypeOfExpense = findViewById(R.id.spinnerExpenseType) // Initialize spinner
        buttonSave = findViewById(R.id.buttonSave)

        expenseDao = MyApp.getDatabase(this).expenseDao()
        typeOfExpenseDao = MyApp.getDatabase(this).typeOfExpenseDao()

        expenseId = intent.getLongExtra(EXTRA_EXPENSE_ID, -1)

        // Load type of expenses into spinner
        GlobalScope.launch(Dispatchers.IO) {
            val typeOfExpenses = typeOfExpenseDao.getAllTypeOfExpenses()
            withContext(Dispatchers.Main) {
                val adapter = ArrayAdapter(this@EditActivity, android.R.layout.simple_spinner_item, typeOfExpenses)
                spinnerTypeOfExpense.adapter = adapter

                if (expenseId != -1L) {
                    GlobalScope.launch(Dispatchers.IO) {
                        val expense = expenseDao.getExpenseById(expenseId)
                        withContext(Dispatchers.Main) {
                            expense?.let {
                                editTextName.setText(it.name)
                                editTextAmount.setText(it.amount.toString())
                                spinnerTypeOfExpense.setSelection(getSpinnerIndex(typeOfExpenses, it.typeOfExpenseId))
                            }
                        }
                    }
                }
            }
        }

        buttonSave.setOnClickListener {
            saveExpense()
        }
    }


    private fun saveExpense() {
        val name = editTextName.text.toString()
        val amount = editTextAmount.text.toString().toDouble()
        val selectedTypeOfExpense = spinnerTypeOfExpense.selectedItem as TypeOfExpense

        if (name.isNotEmpty()) {
            val expense = Expense(id = expenseId, name = name, amount = amount, typeOfExpenseId = selectedTypeOfExpense.id)
            GlobalScope.launch(Dispatchers.IO) {
                expenseDao.updateExpense(expense)
                withContext(Dispatchers.Main) {
                    // Notify ListActivity to refresh the expenses
                    val refreshIntent = Intent(this@EditActivity, ListActivity::class.java)
                    refreshIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
                    startActivity(refreshIntent)
                    finish()
                }
            }
        }
    }

    private fun getSpinnerIndex(typeOfExpenses: List<TypeOfExpense>, typeOfExpenseId: Long): Int {
        for ((index, typeOfExpense) in typeOfExpenses.withIndex()) {
            if (typeOfExpense.id == typeOfExpenseId) {
                return index
            }
        }
        return 0
    }
}

