package com.example.financije
import android.content.Intent
import android.os.Bundle
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch


class MainActivity : AppCompatActivity() {

    private lateinit var buttonViewExpenses: Button
    private lateinit var btnSum: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        buttonViewExpenses = findViewById(R.id.buttonViewExpenses)

        btnSum = findViewById(R.id.btnSum)


        buttonViewExpenses.setOnClickListener {
            val intent = Intent(this, ListActivity::class.java)
            startActivity(intent)
        }

        btnSum.setOnClickListener {
            val intent = Intent(this, UkupnoActivity::class.java)
            startActivity(intent)
        }

        val expenseDatabase = ExpenseDatabase.getInstance(applicationContext)
        val expenseDao = expenseDatabase.expenseDao()
        val typeOfExpenseDao = expenseDatabase.typeOfExpenseDao()

        // Perform database operations on a background thread
        GlobalScope.launch(Dispatchers.IO) {
            val expenses = expenseDao.getAllExpenses()
            // Do something with expenses

            val typeOfExpenses = typeOfExpenseDao.getAllTypeOfExpenses()
            // Do something with typeOfExpenses

            // Insert multiple type of expenses


        }

    }
}
