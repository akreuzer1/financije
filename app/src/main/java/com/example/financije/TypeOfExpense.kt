package com.example.financije

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "type_of_expenses")
data class TypeOfExpense(
    @PrimaryKey(autoGenerate = true)
    val id: Long = 0,
    val name: String
)

