package com.example.financije

import android.app.Application
import android.content.Context
import androidx.room.Room

class MyApp : Application() {
    companion object {
        private const val DATABASE_NAME = "expense_database_v2"
        private lateinit var database: ExpenseDatabase

        fun getDatabase(context: Context): ExpenseDatabase {
            if (!::database.isInitialized) {
                database = Room.databaseBuilder(
                    context.applicationContext,
                    ExpenseDatabase::class.java,
                    DATABASE_NAME
                ).build()
            }
            return database
        }
    }
}
