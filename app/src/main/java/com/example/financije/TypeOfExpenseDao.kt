package com.example.financije

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query

@Dao
interface TypeOfExpenseDao {
    @Query("SELECT * FROM type_of_expenses")
    fun getAllTypeOfExpenses(): List<TypeOfExpense>
/*
    @Query("SELECT * FROM type_of_expenses WHERE id = :id")
    fun getTypeOfExpenseById(id: Long): Expense

*/

    @Insert
    fun addTypeOfExpenses(typeOfExpenses: List<TypeOfExpense>)
}
